      program test_char
        integer :: i = 74
        character(1) :: c
        character(4) :: G
        character(1) :: t
        c = char(i)
        G = 'This'
        t = '24'
        write(*,'(A,I2,A)')'"',i,'"'
        write(*,'(I2,X,A)')i, c ! returns 'J'
        open(611,file='GMJ_'//t//'.csv',action="readwrite")
        write(611,'(I2,X,A)')i, c ! returns 'J'
        CLOSE(611)
      end program test_char
