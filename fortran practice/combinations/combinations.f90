! Combinations of xyz coordinates... start with simple example
! Grier M. Jones
! University of Tennessee, Knoxville
! Fall 2022-
!
! DO NOT do the distance matrix of 19900 x 1600 x 1600
! array... that's 407 GB
! 64 bits in double-precision float
! 8 bits/byte, 1e9 bytes in GB
! ...so do the math
!
subroutine norm(A1,A2,L2)
 ! Euclidean distance between R^3 vectors
 use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
 real(dp),dimension(3) :: A1
 real(dp),dimension(3) :: A2
 real(dp) :: x, y, z
 real(dp),intent(out) :: L2

 x=(A1(1)-A2(1))**2d0
 y=(A1(2)-A2(2))**2d0
 z=(A1(3)-A2(3))**2d0
 
 
 ! Return norm
 L2=sqrt(x+y+z)
 return
END 

program combinations
 ! Get the combinations of xyz coordinates
 use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
 implicit none

 ! Define variables
 character(len=1024) :: header
 character(len=1024) :: string,key1,key2,key3 
 character(len=1024) :: filename 
 integer :: i,j,k,l
 integer :: m,n,o,p
 integer :: idx
 integer process_Rank, size_Of_Cluster, ierror
 integer :: beadtotal,numrings
 logical :: debug,check
 logical :: logicalidx
 real(dp)  :: x1, y1, z1
 real(dp) :: L2
 real(sp) :: start, finish
 ! Define arrays
 logical,  dimension(:),allocatable :: logicalA
 real(dp), dimension(:,:),allocatable :: dat
 real(dp), dimension(:,:),allocatable :: dat1
 real(dp), dimension(:,:,:),allocatable :: dat2
 real(dp), dimension(:,:,:),allocatable :: stacked
 real(dp), dimension(:,:),allocatable :: distmat


 ! Logicals for testing
 debug=.false.
 !debug=.true.
             
 ! open file
 open(99,file="N800.0.xyz",status="old",action="read")
 
 ! Read number of beads
 read(99,'(I4)') beadtotal
 numrings = beadtotal/800

 ! Read header
 read(99,"(A)") header
 if (debug.eqv..true.) then
  print*,header
 endif
 
 ! Allocate the arrays for manipulation
 allocate(dat(160000,4))
 allocate(dat1(160000,3))
 allocate(dat2(200,800,3))
 allocate(stacked(19900,1600,3))
 allocate(logicalA(160000))
 
 ! Read initial data
 ! dim(dat) = total number of beads x 4 (i.e. atom type, x, y, z) 
 ! dim(dat1) = total number of beads x 3 (i.e.  x, y, z) 
 DO i=1,beadtotal
  read(99,*)dat(i,:)
  dat1(i,:)=dat(i,2:4)
 enddo
 
 ! Close xyz file
 close(99)

 ! dim(dat2) = number of polymers x number of beads/polymer x (x,y,z) 
 dat2=reshape(dat1,[200,800,3],order=[2,1,3])
 
 ! Check to make sure the elements of dat2=dat1 
  idx=1
  DO i=1,200
   DO j=1,800
    logicalidx = all(dat2(i,j,:) == dat1(idx,:))
    logicalA(idx)=logicalidx
    if (logicalidx.eqv..false.) then
     print'(4F8.4)',dat2(i,j,:)
     print'(4F8.4)',dat1(idx,:)
    endif
    idx=idx+1          
   ENDDO
  ENDDO
  check = all(logicalA.eqv..true.)
  if (check.eqv..true.) then
   print'(A)','All values passed'
  else
   print'(A)','A value failed, check code!'
  end if

 ! Generate unique combinations of stacked xyz coordinates
 idx=1
 ! Key should be idx_k_l, idx={1,...,19900}
 open(199,file='key.txt',action='write') 
 DO k=1,200
  DO l=k+1,200
   write(key1,'(I5.5)')idx 
   write(key2,'(I5.5)')k
   write(key3,'(I5.5)')l
   write(199,'(A)')trim(key1)//"_"//trim(key2)//"_"//trim(key3)
   stacked(idx,1:800,:)=dat2(k,1:800,:)
   stacked(idx,801:1600,:)=dat2(l,1:800,:)
   idx=idx+1
  END DO
 END DO
 close(199)

 ! If debug, print the size of each array in gigabyte 
 if (debug.eqv..true.) then
  print'(A,1E10.4,A)','stacked ', sizeof(stacked)/1e9 ,' GB'
  print'(A,1E10.4,A)','dat ', sizeof(dat)/1e9 ,' GB'
  print'(A,1E10.4,A)','dat1 ', sizeof(dat1)/1e9 ,' GB'
  print'(A,1E10.4,A)','dat2 ', sizeof(dat2)/1e9 ,' GB'
 endif
 
 ! Deallocate previous arrays 
 deallocate(dat)
 deallocate(dat1)
 deallocate(dat2)
 deallocate(logicalA)

 !Loop list and then through the coordinates

 !$OMP PARALLEL DO
 if (debug.eqv..true.) then
  call cpu_time(start)
 endif

 DO m=1,12

  !DO m=1,19900
  
  ! Index integer to string and open file
  write(string,'(I5.5)')m
  filename="distmat_"//trim(string)//"_out"
  open(100,file=filename,action="write")
  
  ! allocate the dynamic distance matrix and get the norm 
  allocate(distmat(1600,1600))
  
  DO n=1,1600
   DO o=1,1600
    call norm(stacked(m,n,1:3),stacked(m,o,1:3),L2)
    distmat(n,o)=L2

   ENDDO
   
   ! Write distance matrix to file
   write(100,'(9999(g0))')(distmat(n,o),' ',o=1,1600)
  ENDDO
  
  ! deallocate distmat and close current file
  deallocate(distmat)
  close(100)


  call execute_command_line("ripser "//filename//" > PD_"//trim(string))
  call execute_command_line("rm "//filename )

  
  ! Debug
  if (debug.eqv..true.) then
   print'(A,1E10.4,A)','distmat ', sizeof(distmat)/1e9 ,' GB'
  endif


 ENDDO
 !$omp end parallel do
 ! deallocate stacked finally
 deallocate(stacked)

 if (debug.eqv..true.) then
  call cpu_time(finish)
  print '("Time = ",f6.3," seconds.")',finish-start
 endif

end program combinations
