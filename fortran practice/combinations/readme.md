To compile

```
gfortran -openmp combinations.f90 -o combinations
```

To run
```
OMP_NUM_THREADS=4 ./combinations
