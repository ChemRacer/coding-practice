! Combinations of xyz coordinates... start with simple example
! Grier M. Jones
! University of Tennessee, Knoxville
! Fall 2022-
!
! DO NOT do the distance matrix of 19900 x 1600 x 1600
! array... that's 407 GB
! 64 bits in double-precision float
! 8 bits/byte, 1e9 bytes in GB
! ...so do the math
!


program combinations
 ! Get the combinations of xyz coordinates
 use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
 implicit none
 
 ! Define variables
 character(len=1024) :: string,key1,key2,key3 
 character(len=1024) :: filename,file1,dirname,xyzfilename 

 integer :: i,j,k,l
 integer :: m,n,o,p
 integer :: idx
 integer :: unit1,unit2,unit3 
 integer :: beadtotal,numrings
 integer :: thread_id
 logical :: check,xyz_e
 logical :: logicalidx,dir_e
 real(dp) :: L2
 real(sp) :: start, finish
 integer :: num,io_stat
 real(dp) :: var1, var2, var3
 namelist /mydata/ num, var1, var2, var3 
 
 REAL(dp) :: Times1,Times2
 INTEGER :: iTimes1,iTimes2, rate 
 ! Define arrays
 logical,  dimension(:),allocatable :: logicalA
 real(dp), dimension(:,:),allocatable :: dat
 real(dp), dimension(:,:),allocatable :: dat1
 real(dp), dimension(:,:,:),allocatable :: dat2
 real(dp), dimension(:,:,:),allocatable :: stacked
 real(dp), dimension(:,:),allocatable :: distmat
 



 CALL system_clock(count_rate=rate)

 call CPU_TIME(Times1)
 call SYSTEM_CLOCK(iTimes1)
 write(*,*) 'CPU SERIAL STARTED:',Times1 

 unit1 = 11
 unit2 = 21
 unit3 = 31

 DO p=0,7
 !DO p=0,1000
  write(file1,'(I4)')p

  print*,'FILE ',trim(adjustl(file1)) 
  ! Create save directory
  dirname=trim(adjustl("N800_"//trim(ADJUSTL(file1))))
  xyzfilename="cleaned_files/N800."//trim(adjustl(file1))//".xyz"
  
  ! Check file
  inquire(file=dirname, exist=dir_e)
  if ( dir_e ) then
   write(*,*)trim(adjustl(dirname))," Dir exists!"
   call execute_command_line("rm -rf "//trim(adjustl(dirname)))
   call execute_command_line("mkdir "//trim(adjustl(dirname)))
   call run(xyzfilename,unit1,unit2,unit3,thread_id,file1) 
  else
   write(*,*)'MKDIR ',trim(adjustl(dirname))
   call execute_command_line("mkdir "//trim(adjustl(dirname)))
   call run(xyzfilename,unit1,unit2,unit3,thread_id,file1) 
  endif 
ENDDO

call CPU_TIME(Times2)
call SYSTEM_CLOCK(iTimes2) 
write(*,*) 'CPU SERIAL finished:',Times2
write(*,*) 'SERIAL TIMES:',Times2-Times1, real(iTimes2-iTimes1)/real(rate)
contains 
 subroutine norm(A1,A2,L2)
  ! Euclidean distance between R^3 vectors
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  real(dp),dimension(3) :: A1
  real(dp),dimension(3) :: A2
  real(dp) :: x, y, z
  real(dp),intent(out) :: L2
 
  x=(A1(1)-A2(1))**2d0
  y=(A1(2)-A2(2))**2d0
  z=(A1(3)-A2(3))**2d0
  
  ! Return norm
  L2=sqrt(x+y+z)
  return
 END 
 
 subroutine run(xyzfilename,unit1,unit2,unit3,thread_id,file1) 
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  
  ! Define variables
  character(len=1024) :: string,key1,key2,key3 
  character(len=1024) :: filename,file1,dirname,xyzfilename 
 
  integer :: i,j,k,l
  integer :: m,n,o,p
  integer :: idx
  integer :: unit1,unit2,unit3
  integer :: beadtotal,numrings
  integer :: thread_id
  logical :: check,xyz_e
  logical :: logicalidx,dir_e
  real(dp) :: L2

  integer ::  io_stat
  ! Define arrays
  logical,  dimension(:),allocatable :: logicalA
  real(dp), dimension(:,:),allocatable :: dat,datb
  real(dp), dimension(:,:),allocatable :: dat1
  real(dp), dimension(:,:,:),allocatable :: dat2
  real(dp), dimension(:,:,:),allocatable :: stacked
  real(dp), dimension(:,:),allocatable :: distmat
  


  ! Allocate the arrays for manipulation
  allocate(dat(160000,4))
  allocate(datb(160000,4))
  allocate(dat1(160000,3))
  allocate(dat2(200,800,3))
  allocate(logicalA(160000))
  allocate(stacked(19900,1600,3))
  
  ! open file
               
  inquire(file=xyzfilename, exist=xyz_e)
               
  if ( xyz_e ) then
   print'(A)',trim(adjustl(xyzfilename))//' Exists'
   !open xyz file 
   open(unit1,file=xyzfilename,action="read",status='old',form='formatted',access='sequential') 

   ! Number of beads 
   beadtotal=160000
 

   print'(A,1I4,X,1I6)','on thread',thread_id,beadtotal 
   io_stat=0
   DO i=1,beadtotal
    read(unit1,*, iostat=io_stat)dat(i,:)
    if (io_stat /= 0) exit
    datb(i,:)=dat(i,:)
   !print'(A,1I1,X,1I6,X,1I6,X,4F12.8)','Loop: ',thread_id,i,io_stat,dat(i,:)
   enddo
 
!  DO i=1,beadtotal
!   print'(A,1I1,X,1I6,X,1I6,X,4F12.8)','Loop: ',thread_id,i,io_stat,datb(i,:)
!  enddo

  
   numrings = beadtotal/800
  
   ! Read initial data
   ! dim(dat) = total number of beads x 4 (i.e. atom type, x, y, z) 
   ! dim(dat1) = total number of beads x 3 (i.e.  x, y, z) 
   DO i=1,beadtotal
    dat1(i,:)=datb(i,2:4)
   enddo
   ! Close xyz file
   close(unit1)
   
   ! dim(dat2) = number of polymers x number of beads/polymer x (x,y,z) 
   dat2=reshape(dat1,[200,800,3],order=[2,1,3])
   
   ! Check to make sure the elements of dat2=dat1 
   idx=1
   DO i=1,200
    DO j=1,800
     logicalidx = all(dat2(i,j,:) == dat1(idx,:))
     logicalA(idx)=logicalidx
     if (logicalidx.eqv..false.) then
      print'(4F8.4)',dat2(i,j,:)
      print'(4F8.4)',dat1(idx,:)
     endif
     idx=idx+1          
    ENDDO
   ENDDO
  end if
  
  check = all(logicalA.eqv..true.)
  if (check.eqv..true.) then
   print'(A)','All values passed'
  else
   print'(A)','A value failed, check code!'
  end if

  ! Generate unique combinations of stacked xyz coordinates
  idx=1
  ! Key should be idx_k_l, idx={1,...,19900}
  open(unit2,file='key.txt',action='write') 
  DO k=1,200
   DO l=k+1,200
    write(key1,'(I5.5)')idx 
    write(key2,'(I5.5)')k
    write(key3,'(I5.5)')l
    write(unit2,'(A)')trim(key1)//"_"//trim(key2)//"_"//trim(key3)
    stacked(idx,1:800,:)=dat2(k,1:800,:)
    stacked(idx,801:1600,:)=dat2(l,1:800,:)
    idx=idx+1
   END DO
  END DO
  close(unit2)
  

  
  !DO m=1,19900
  DO m=1,2
   ! Index integer to string and open file
   write(string,'(I5.5)')m
   filename=trim(adjustl("distmat_"//trim(adjustl(string))//"_out"))
   
   open(unit3,file=filename,action="write")
   ! allocate the dynamic distance matrix and get the norm 
   allocate(distmat(1600,1600))
   
   DO n=1,1600
    DO o=1,1600
     call norm(stacked(m,n,1:3),stacked(m,o,1:3),L2)
     distmat(n,o)=L2
    ENDDO
    ! Write distance matrix to file
    write(unit3,'(9999(g0))')(distmat(n,o),' ',o=1,1600)
   ENDDO
    
   ! deallocate distmat and close current file
   deallocate(distmat)
   close(unit3)

    call execute_command_line(trim(adjustl("ripser "//filename//" > PD_"//trim(adjustl(string)))))
    call execute_command_line(trim(adjustl("mv PD_"//trim(adjustl(string))//" N800_"//trim(ADJUSTL(file1)))))
    call execute_command_line(trim(adjustl(("rm "//filename))))

  ENDDO

  
  ! Deallocate previous arrays 
  deallocate(dat)
  deallocate(datb)
  deallocate(dat1)
  deallocate(dat2)
  deallocate(logicalA)
  deallocate(stacked)
  
 END 
end program combinations
