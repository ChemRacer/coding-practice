program test
implicit none
include 'mpif.h'
integer, parameter :: dp=kind(1.0d0)
real(dp), dimension(8,2) :: array1
integer :: i,j,nprocs,ierr,myrank
real(dp) :: s
real(dp), dimension(8) :: stmp
call mpi_init(ierr)
call mpi_comm_size(mpi_comm_world,nprocs,ierr)
call mpi_comm_rank(mpi_comm_world,myrank,ierr)
array1 = 1.11111111111111_dp
stmp = 0.0d0
!call start_collection("region1")
!ocl simd
do i=1,100000
do j=1,125000 ! 25 ! 100
stmp(1) = stmp(1) +array1(1,1)*array1(1,2)
stmp(2) = stmp(2) +array1(2,1)*array1(2,2)
stmp(3) = stmp(3) +array1(3,1)*array1(3,2)
stmp(4) = stmp(4) +array1(4,1)*array1(4,2)
stmp(5) = stmp(5) +array1(5,1)*array1(5,2)
stmp(6) = stmp(6) +array1(6,1)*array1(6,2)
stmp(7) = stmp(7) +array1(7,1)*array1(7,2)
stmp(8) = stmp(8) +array1(8,1)*array1(8,2)
enddo
enddo
!call stop_collection("region1")
s = stmp(1) +stmp(2) +stmp(3) +stmp(4) +stmp(5) +stmp(6)&
+stmp(7)+stmp(8)
call mpi_finalize(ierr)
end program test
