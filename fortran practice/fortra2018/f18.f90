! L2 Norm of a vector
! Modern fortran
function vector_norm(n,vec) result(norm)
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  integer, intent(in) :: n
  real, intent(in) :: vec(n)
  real(dp) :: norm

  norm = sqrt(sum(vec**2))

end function vector_norm

program run_fcn
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  real :: v(9)
  real(dp) :: vector_norm

  v(:) = 9

  print *, 'Vector norm = ', vector_norm(9,v)

end program run_fcn
