# FORTRAN example
cmake_minimum_required(VERSION 3.5)

project(hello)
enable_language(Fortran)

if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
    set(dialect "-ffree-form -std=f2008 -fimplicit-none")
    set(bounds "-fbounds-check")
endif()
if(CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
    set(dialect "-stand f08 -free -implicitnone")
    set(bounds "-check bounds")
endif()
if(CMAKE_Fortran_COMPILER_ID MATCHES "PGI")
    set(dialect "-Mfreeform -Mdclchk -Mstandard -Mallocatable=03")
    set(bounds "-C")
endif()
# Support for HDF5 I/O library
#=============================

if (HDF5)
        if (ADDRMODE EQUAL 32)
                message ("-- HDF5 interface not compatible with 32bit installations")
        else()
                message ("Configuring HDF5 support:")
                get_property (languages GLOBAL PROPERTY ENABLED_LANGUAGES)
                if (";${languages};" MATCHES ";CXX;")
                        find_package (HDF5 COMPONENTS C CXX)
                else ()
                        find_package (HDF5 COMPONENTS C)
                endif ()
                if (HDF5_FOUND)
                        message ("-- HDF5_INCLUDE_PATH: ${HDF5_INCLUDE_DIRS}")
                        message ("-- HDF5_C_LIBRARIES: ${HDF5_C_LIBRARIES}")
                        list (APPEND EXTERNAL_LIBRARIES ${HDF5_C_LIBRARIES})
                        if (HDF5_CXX_LIBRARIES)
                                message ("-- HDF5_CXX_LIBRARIES: ${HDF5_CXX_LIBRARIES}")
                                list (APPEND EXTERNAL_LIBRARIES ${HDF5_CXX_LIBRARIES})
                        endif ()
                else()
                        message ("-- HDF5 not found, it will be deactivated")
                endif()
        endif ()
endif ()
set(CMAKE_Fortran_FLAGS_DEBUG "${CMAKE_Fortran_FLAGS_DEBUG} ${bounds}")
set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${dialect}")

#
# Compile.
#
file(GLOB_RECURSE sources  src/*.f src/*.h)
add_executable(prog ${sources})

