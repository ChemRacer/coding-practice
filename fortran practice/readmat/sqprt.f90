program readfile
 implicit none
 double precision, dimension(:,:), allocatable :: array
 double precision, dimension(:,:), allocatable :: arr2
 double precision, dimension(:,:), allocatable :: arr3
 integer :: i,j,ind
 allocate(array(2,2))
 allocate(arr2(2,2))
 allocate(arr3(2,2))
 ind=0
 DO i=1,2
  DO j=1,2
   ind=ind+1
   arr2(i,j)=1.d0
   arr3(i,j)=1.d0*ind
   if (i.eq.j) then
    array(i,j)=1.d0
   ELSE
    array(i,j)=ind+1.d0
   END IF
  END DO
 END DO


 DO i=1,2
  print'(A,2I2)','arr3',(int(arr3(i,j)),j=1,2)
 END DO


 print*
 DO i=1,2
  print'(A,2I2)','arr2',(int(arr2(i,j)),j=1,2)
 END DO
 print*
 DO i=1,2
  print'(A,2I2)','array',(int(array(i,j)),j=1,2)
 END DO

 call SQUARE(array,arr2,1,2,2)
 print*
 DO i=1,2
  print'(A,2I2)','square arr2',(int(arr2(i,j)),j=1,2)
 END DO
 print*,shape(array),shape(arr2),shape(arr3)
 call mxmt(array,2,1,arr2,1,2,arr3,2,2)
 print*
 DO i=1,2
  print'(A,4I4)','mxmt arr3',(int(arr3(i,j)),j=1,2)
 END DO
 print*
 deallocate(array)
 deallocate(arr2)
 deallocate(arr3)
 END PROGRAM
