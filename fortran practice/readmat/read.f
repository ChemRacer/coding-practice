! Fix weird read issue
! FORTRAN was reading array sequentially and flattening it...
! which isn't what we want
      program readfile
       implicit none
       DOUBLE PRECISION, dimension(:), allocatable :: array
       DOUBLE PRECISION, dimension(:,:), allocatable :: arr2
       character(len=4) :: mystring
       integer :: i,N,dims,k,filenumber,j
       N=2
       dims=N*(N+1)/2
       allocate(array(dims*N*N))
       allocate(arr2(dims,N*N))
       write(mystring,'(I0)')N*N

       open(9,file='GMJ_AO_INT.csv', action="readwrite")
       read(9,*)array
       array=reshape(array,(/N*N*dims/))
       arr2=transpose(reshape(array,(/N*N,dims/)))
       print'(A,2I4)','SHAPE of array',shape(array)
       print'(A,2I2)','CHECK dims, N*N',dims,N*N
       print'(A,2I2)','Check new array shape',shape(arr2)
       print*
       print'(A)','Original array'
       DO i=1,N*N*dims
         write(6,'(E14.4,2X)'),array(i)
       END DO
       print*
       print'(A)','Array we want'
       DO i=1,dims
        write(6,'('//mystring//'E14.4,2X)')
     &      (arr2(i,j),j=1,N*N)
       END DO

       close(9)
       deallocate(array)
       deallocate(arr2)
      END PROGRAM
