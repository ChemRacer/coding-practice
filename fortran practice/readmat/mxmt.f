************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
************************************************************************
      SUBROUTINE MXMT(A,ICA,IRA, B,ICB,IRB, C, NROW,NSUM)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION A(*),B(*),C(*)
      IND=0
      DO 100 IROW=0,NROW-1
      DO 100 ICOL=0,IROW
         SUM=0.0D0
         DO 110 ISUM=0,NSUM-1
            print'(A,1I4)','A INDEX',1+IROW*ICA+ISUM*IRA
c           print'(A,5I3)','A components',1,IROW,ICA,ISUM,IRA
            print'(A,1I4)','A VAL',int(A(1+IROW*ICA+ISUM*IRA))
            print*
            print'(A,1I4)','B INDEX',1+ISUM*ICB+ICOL*IRB
c           print'(A,5I3)','B components',1,ISUM,ICB,ICOL,IRB
            print'(A,1I4)','B VAL',int(B(1+ISUM*ICB+ICOL*IRB))
            AB=A(1+IROW*ICA+ISUM*IRA) *
     &         B(1+ISUM*ICB+ICOL*IRB)
            print'(A,1I4)','AB',int(AB)
            SUM=SUM+AB
            print'(A,1I4)','SUM',int(SUM)
110      CONTINUE
         IND=IND+1
         print'(A,2I4)','C !!!',IND,int(SUM)
         print*
         C(IND)=SUM
100   CONTINUE
      RETURN
      END
