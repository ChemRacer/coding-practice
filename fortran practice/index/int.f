c GMJ 4/17/20 Test with a code that determines if a file exists

         program FileTest
          IMPLICIT none
          integer NBAS,i,ijidx,klidx,iorb,jorb,korb,lorb
          NBAS=2
          do i = 1,NBAS
            ijidx = ceiling(-0.5d0+sqrt(2.0d0*i))
            klidx = i - (ijidx-1)*ijidx/2
            iorb = ceiling(-0.5d0+sqrt(2.0d0*ijidx))
            jorb = ijidx - (iorb-1)*iorb/2
            korb = ceiling(-0.5d0+sqrt(2.0d0*klidx))
            lorb = klidx - (korb-1)*korb/2
            write(*,'(4I1)')iorb,jorb,korb,lorb
          END DO
         end program

