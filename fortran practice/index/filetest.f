      program Mmul
c GMJ 4/16/21
c Use this one for case 1-9
        implicit none
        integer :: unitNumber,ICASE
        integer :: m,n
        character (len=2) :: filetype
        character (2) :: mystring
        ICASE=2
        n=1
        m=2
        filetype='e2'
        write(mystring,'(I0.2)')ICASE
        OPEN(unitNumber,
     &  file='GMJ_'//filetype//'_'//mystring//'.csv',
     &  action='readwrite')
        write(unitNumber,'(I5,1X,A,I5)')n,'x',m
        CLOSE(unitNumber)
      RETURN
      END
