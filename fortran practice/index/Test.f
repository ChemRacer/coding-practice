c GMJ 4/17/20 Test with a code that determines if a file exists

         program FileTest
          IMPLICIT none
          integer A,I,V
          character(LEN=40) :: frmat
          character(LEN=40) :: smlfrmat
          A=1
          I=12
          V=12
c Iw.m
c w: the number of positions to be used
c m: the minimum number of positions to be used
c I0.3 allows up to 3 integers and fills the space with 0
          frmat='(A,I0.3,A,I0.3,A,I0.3,A,I0.3)'
          smlfrmat='(A,I1.1,A,I1.1,A,I1.1,A,I1.1)'
          IF(V.GE.10.and.I.ge.10) THEN
             write(*,frmat)'I',I,'_A',A,'_V',
     &       V,'_A',A
          else
             write(*,smlfrmat)'I',I,'_A',A,'_V',
     &       V,'_A',A

          END IF


         end program

