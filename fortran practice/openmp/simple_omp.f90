program simple
implicit none
integer,parameter :: n
integer :: array(n)

n=100
print *,n
!$omp parallel do
do i = 1, n
   array(i) = sqrt(real(i))
enddo
!$omp end parallel do

end program
