      subroutine int2char(x)
        implicit none
        integer :: x
        write(*,'(A,I1,A)')'"',x,'"'
        RETURN
      END

      subroutine inttochar(x)
        implicit none
        integer :: x
        character (1) :: mystring
        write(mystring,'(I1)')x
        write(*,*)mystring
        open(x,file='File'//mystring//'.csv',action='readwrite')
        write(x,'(A)')'Test Test'
        CLOSE(x)
        RETURN
      END

      program test
       call int2char(5)
       call inttochar(5)
      end program test
