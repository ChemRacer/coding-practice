      program Double
       implicit double precision (A-H,O-Z)
       write(*,*)'Test Double Precision'
       Do i=1,13
        write(*,*)i*2.5E-16
       end do
       write(*,*)'Test Real'
       call Real
       write(*,*)'Format Test'
       call FormatTest
       write(*,*)'Sqrt of 4'
       call sq
      end program


      subroutine sq
       implicit real*8 (A-H,O-Z)
       A=4
       write(*,*)sqrt(A)
      return
      end

      subroutine Real
      implicit real*8 (A-H,O-Z)
       Do i=1,13
        write(*,*)i*2.5E-16
       end do
      return
      end

      subroutine FormatTest
      implicit real*8 (A-H,O-Z)
       Do i=1,13
        write(*,'(T1,2X,ES12.5)')i*2.5E-16
       end do
      return
      end
