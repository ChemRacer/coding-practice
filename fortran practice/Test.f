c GMJ 4/17/20 Test with a code that determines if a file exists

         program FileTest
           implicit none
           logical :: exist
           inquire(file="test.txt", exist=exist)
           if (exist) then
             open(12, file="test.txt", status="old",
     &        FORM='FORMATTED',action="readwrite")
           else
             open(12, file="test.txt", status="new", FORM='FORMATTED',
     &        action="readwrite")
           end if
           write(12, *) "SOME TEXT"
           close(12)

         end program

cc GMJ   11/1/19
cc       This is a fortran test program in order to understand some of the
cc       nuances of fortran and the statement labels
cc       To compile: gfortran Test.f -o Test
c        program hello
c        integer i, n, sum
c        n = 9
c        sum = 0
c        do i = 1, n
c           sum = sum + i
c           write(*,"(A,I1,T8,E8.2)") 'i =',i, i*0.5d0
c           write(*,'(T1,F5.3)')i*0.5d0
cc          write(*,*) 'sum =', sum
c        ENDDO
c
c        call TWO_EL
c        end program hello
c         SUBROUTINE TWO_EL
c          integer nacpar
c          REAL ijidx, klidx, iorb, jorb, korb, lorb
c          NACPAR = 4
c          NASHT = 2
c          NACPAR=((NASHT)+(NASHT)**2)/2
c          NACPR2=(NACPAR+NACPAR**2)/2
c
c          write(*,*)'1-elec'
c          DO k = 1, NACPAR
c            iorb = ceiling(-0.5d0+sqrt(2.0d0*k))
c            jorb = k - (iorb-1)*iorb/2
c            write(*,*)'iorb',',',iorb,',','jorb',',',jorb
c          ENDDO
c
c          write(*,*)' '
c
c          write(*,*)'2-ele'
c          DO k = 1, NACPR2
c            ijidx = ceiling(-0.5d0+sqrt(2.0d0*k))
c            klidx = k - (ijidx-1)*ijidx/2
c            iorb = ceiling(-0.5d0+sqrt(2.0d0*ijidx))
c            jorb = ijidx - (iorb-1)*iorb/2
c            korb = ceiling(-0.5d0+sqrt(2.0d0*klidx))
c            lorb = klidx - (korb-1)*korb/2
c            write(*,*)'two electron',',',iorb,',',jorb,',',korb,',',
c     &                  lorb
c          ENDDO
c       END
