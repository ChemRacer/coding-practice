      subroutine foo(x)
      implicit none
      real, intent(in)  :: x(:)
      real              :: y(size(x))
      real, allocatable :: z(:)
      integer           :: ierr
      allocate (z(size(x)),stat=ierr)

      if (ierr/=0) then
         print*,"could not allocate z in foo"
         return
      end if
      end subroutine foo
