      PROGRAM realAdd
      IMPLICIT NONE
      INTEGER,PARAMETER :: RP = SELECTED_REAL_KIND(15)
      REAL(KIND=RP) :: x,y,z
      REAL(KIND=RP),PARAMETER :: pi = 4.0_RP*ATAN(1.0_RP)
       !  Let’s add some numbers
      x = pi
      y = 2.5_RP
      z=x+y
      PRINT*,z
      END PROGRAM realAdd
