#include <iostream>

int main() {

  int power = 9000;

  // Print power
  std::cout << power << "\n";

  // This is a memory address represented in hexadecimal.
  // A memory address is usually denoted in hexadecimal instead of binary for readability and conciseness.
  // Print &power
  std::cout << &power << "\n";

}
