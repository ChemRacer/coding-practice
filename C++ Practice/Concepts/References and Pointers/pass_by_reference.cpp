#include <iostream>
// Pass by arguments allow us to:
// Modify the value of the function arguments.
// Avoid making copies of a variable/object for performance reasons.
int triple(int &i) {

  i = i * 3;

  return i;

}

int main() {

  int num = 1;

  std::cout << triple(num) << "\n";
  std::cout << triple(num) << "\n";

}
