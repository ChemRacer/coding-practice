Memory Allocation
=========
Practice references & pointers in C++.

-Notes on references and pointers:
1. In C++, a reference variable is an alias for something else, that is, another name for an already existing variable.
2. Pass-by-reference refers to passing parameters to a function by using references. When called, the function can modify the value of the arguments by using the reference passed in.
3. A memory address is represented in hexadecimal. A memory address is usually denoted in hexadecimal instead of binary for readability and conciseness.
4. The double meaning of the & symbol can be tricky at first, so make sure to note: 1) When & is used in a declaration, it is a reference operator. 2) When & is not used in a declaration, it is an address operator.
