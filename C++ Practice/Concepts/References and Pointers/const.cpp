#include <iostream>
//Pass by reference with const:
//const keywords tells the compiler that we won’t change something

int square(int const &i) {

  return i * i;

}

int main() {

  int side = 5;

  std::cout << square(side) << "\n";

}
