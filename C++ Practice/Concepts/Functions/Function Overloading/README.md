Function Overloading
=========
Practice linking and compiling files, the use of header files, and function overloading in C++.

-------
1. To compile:

```
g++ -o myprogram main.cpp num_opps.cpp
```
