Classes
=========
Practice linking and compiling files and constructos in C++.

-------
1. To compile creating classes files:

```
g++ music.cpp song.cpp
```

2. A constructor is a special kind of method that lets you decide how the objects of a class get created. It has the same name as the class and no return type. Constructors really shine when you want to instantiate an object with specific attributes.
