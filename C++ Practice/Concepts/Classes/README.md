Classes
=========
Practice linking and compiling files, objects & classes in C++.

-You’ve learned the basics of classes and objects in C++:

1. Classes are user-defined types.
2. Objects are instances of classes.
3. Class members are the data attributes and functions inside of a class.
4. Creating a new object from a class is called instantiation.
5. Class members can be designated as either private or public — they are private by default.
6. You can create a constructor to instantiate objects in a particular way.
7. A destructor allows you to execute any cleanup necessary before an object gets destroyed.


-------
-To compile creating classes files:

```
g++ music.cpp song.cpp
```

