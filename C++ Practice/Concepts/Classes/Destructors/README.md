Classes
=========
Practice linking and compiling files and Destructors in C++.

-------
1. To compile creating classes files:

```
g++ music.cpp song.cpp
```

2. Object destruction is really about tidying up and preventing memory leaks. A destructor is a special method that handles object destruction. Like a constructor, it has the same name as the class and no return type, but is preceded by a ~ operator and takes no parameters
