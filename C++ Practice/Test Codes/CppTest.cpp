/*
 * This is a multiline comment in c++
 */
#include <iostream>
#include <vector>
// An example of a function called below
// This function just tells them in what year they'll turn 21
void garbage(std::string name,int a, int b){
	std::cout<<name<<" can participate in "<<a+b<<"! \n";
}
// This function asks the user what beer they'd like...
// in the future it will take the input and give them the beer of their choice
void choice(){
	std::string beers[3]={"PBR \n","Coors \n","Budweiser \n"};
	std::cout<<"What beer would you like? \n";
	for (int i=0; i<3;i++)
		std::cout<<beers[i]<<"\n";
}

int main(){
//This is a declared and initialized variable
	int year=2020;
	int age=0;
	std::string name;

//Say what year it is and ask how old
        std::cout<<"The year is: "<< year << "\n";
	std::cout<<"What is your name? ";
	std::cin>>name;
	std::cout<<"How old are you? ";
	std::cin>>age;
//Set new_age to age and then determine how many years until 21
	int new_age=age;
	while(new_age<21){
		new_age+=1;
	}
//An if/else statement to determine by age who can participate
	if (age>=21){
		std::cout<<"You are "<<age<<" years old \n";
		std::cout<<"You may participate \n \n";
		choice();
	}
	else{
		std::cout<<"You are too young to participate!\n";
		std::cout<<"Years until participation: "<<new_age-age<<"\n";
//Call the new function
		garbage(name,new_age-age,year);
	}


}
