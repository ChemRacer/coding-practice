Hartree-Fock and VQE code
=========

---------

1. HF from https://nznano.blogspot.com/2018/03/simple-quantum-chemistry-hartree-fock.html
2. VQE from https://qiskit.org/textbook/ch-applications/vqe-molecules.html
