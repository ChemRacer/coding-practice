#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel, RBF
from modAL.models import ActiveLearner
# https://modal-python.readthedocs.io/en/latest/content/examples/active_regression.html
# Try to learn the noisy sin function
X = np.random.choice(np.linspace(0, 20, 10000), size=400, replace=False).reshape(-1, 1)
z = np.random.choice(np.linspace(0, 20, 10000), size=200, replace=False).reshape(-1, 1)
y = np.sin(z) + np.random.normal(scale=0.3, size=z.shape)
X=X.reshape(-1,2)
print(X.shape, y.shape)
##with plt.style.context('seaborn-white'):
##    plt.figure(figsize=(10, 5))
##    plt.scatter(X, y, c='k', s=20)
##    plt.title('sin(x) + noise')
##    plt.show()

def GP_regression_std(regressor, X):
    _, std = regressor.predict(X, return_std=True)
    query_idx = np.argmax(std)
    return query_idx, X[query_idx]

n_initial = 5
initial_idx = np.random.choice(range(len(X)), size=n_initial, replace=False)
X_training, y_training = X[initial_idx], y[initial_idx]

kernel = RBF(length_scale=1.0, length_scale_bounds=(1e-2, 1e3)) \
         + WhiteKernel(noise_level=1, noise_level_bounds=(1e-10, 1e+1))
print(X_training.reshape(-1, 1).shape,X_training.shape)
print(y_training.reshape(-1, 1).shape,y_training.shape)
regressor = ActiveLearner(
    estimator=GaussianProcessRegressor(kernel=kernel),
    query_strategy=GP_regression_std,
    X_training=X_training, y_training=y_training.reshape(-1, 1)
)

X_grid = np.linspace(0, 20, 1000).reshape(-1,2)
print(X_grid.shape)
y_pred, y_std = regressor.predict(X_grid, return_std=True)
y_pred, y_std = y_pred.ravel(), y_std.ravel()

#with plt.style.context('seaborn-white'):
#    plt.figure(figsize=(10, 5))
#    plt.plot(X_grid, y_pred)
#    plt.fill_between(X_grid, y_pred - y_std, y_pred + y_std, alpha=0.2)
#    plt.scatter(X, y, c='k', s=20)
#    plt.title('Initial prediction')
#    plt.show()

n_queries = 10
for idx in range(n_queries):
    query_idx, query_instance = regressor.query(X)
    print(query_idx, query_instance)
    print(X[query_idx].reshape(1, -1).shape, y[query_idx].reshape(1, -1).shape)
    regressor.teach(X[query_idx].reshape(1, -1), y[query_idx].reshape(1, -1))


y_pred_final, y_std_final = regressor.predict(X_grid, return_std=True)
y_pred_final, y_std_final = y_pred_final.ravel(), y_std_final.ravel()

#with plt.style.context('seaborn-white'):
#    plt.figure(figsize=(10, 8))
#    plt.plot(X_grid, y_pred_final)
#    plt.fill_between(X_grid, y_pred_final - y_std_final, y_pred_final + y_std_final, alpha=0.2)
#    plt.scatter(X, y, c='k', s=20)
#    plt.title('Prediction after active learning')
#    plt.show()


