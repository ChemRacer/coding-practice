
What is “blockchain”?
========================
Blockchain is a way of storing digital data. The data can literally be anything. For Bitcoin, it’s the transactions (logs of transfers of Bitcoin from one account to another), but it can even be files; it doesn’t matter. The data is stored in the form of blocks, which are linked (or chained) together using cryptographic hashes — hence the name “blockchain.”

All of the magic lies in the way this data is stored and added to the blockchain. A blockchain is essentially a linked list that contains ordered data, with a few constraints such as:

Blocks can’t be modified once added; in other words, it is append only.
There are specific rules for appending data to it.
Its architecture is distributed.
Enforcing these constraints yields the following benefits:

Immutability and durability of data
No single point of control or failure
A verifiable audit trail of the order in which data was added
So, how can these constraints achieve these characteristics? We’ll get more into that as we implement this blockchain. Let’s get started.

src
==========
```
https://developer.ibm.com/technologies/blockchain/tutorials/develop-a-blockchain-application-from-scratch-in-python/

```
