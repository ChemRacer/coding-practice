Directory with Python Projects
=========
Different projects with ML in mind.

1. Format Code to PEP8:
```
yapf --in-place --recursive --style='{based_on_style: pep8, indent_width: 4}' *.py
```
2. Check PEP8 format:
```
pep8 --show-source --show-pep8 *.py
```

OR

```
pep8 --first *.py
```

To install a module from github:
```
pip install git+https://github.com/username/project.git
```
