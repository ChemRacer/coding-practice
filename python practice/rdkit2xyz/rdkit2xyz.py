from rdkit.Chem import AllChem, AddHs,rdmolfiles, MolFromSmiles, AddHs
mol = MolFromSmiles('CCN.C(=O)=O')
mol=AddHs(mol)
AllChem.EmbedMolecule(mol)
AllChem.MMFFOptimizeMoleculeConfs(mol,maxIters=10000,ignoreInterfragInteractions=False)
AllChem.Cleanup(mol)
mol.GetConformer()

with open('mol.xyz','w') as w:
    w.write(f'{mol.GetNumAtoms()}\n\n')
    for i, atom in enumerate(mol.GetAtoms()):
        positions = mol.GetConformer().GetAtomPosition(i)
        w.write(f'{atom.GetSymbol()} {positions.x:>10.6f} {positions.y:>10.6f} {positions.z:>10.6f}\n')
        print(f'{atom.GetSymbol()} {positions.x:>10.6f} {positions.y:>10.6f} {positions.z:>10.6f}')
