from math import sqrt
from joblib import Parallel, delayed, parallel_backend
import threading
from time import sleep, perf_counter

def f(x):
    '''
    Sleep for four seconds
    params
    ------
    x - empty argument
   
    returns
    -------
    None
    '''
    return sleep(4)

# Using threading backend!
# parallel(delayed(function)(argument))
t0=perf_counter()
with parallel_backend('threading', n_jobs=12):
    parallel = Parallel(verbose=100, pre_dispatch='1.5*n_jobs')
    parallel(delayed(f)(i) for i in range(12))
print(f'{perf_counter()-t0:.4f}')


# Serial check
t0=perf_counter()
[f(i) for i in range(12)]
print(f'{perf_counter()-t0:.4f}')
