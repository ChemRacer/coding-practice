## Deep Learning Model to generate text using Keras LSTM
```
git clone https://github.com/Oindrila-Sen/Python-Projects.git
```

From: https://medium.com/mlearning-ai/deep-learning-model-to-generate-text-using-keras-lstm-25ae07b8c2d1

#Grab Books
```
wget  -P /Users/gjonesresearch/Coding\ Practice/python\ practice/TextGen/LSTM_Text_Generator/ "URL"
```

#Use conda env
```
conda activate keras_env
```

Objectives and Junk
========
1. Text can be obtained from https://www.gutenberg.org/ebooks/
2. I'd like to use this AI to make sci-fi stuff
