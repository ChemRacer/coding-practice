#!/bin/python3
import os


def genDirs():
    DataDirs = []
    for i in os.listdir():
        if os.path.exists(os.path.join(str(i), 'MLDataGrab')) is True:
            DataDirs.append(os.path.join(str(i), 'MLDataGrab'))

    SubDirs = [
        'IVECW', 'IVECC2', 'CASPT2Energies', 'hdf5', 'E2', 'PreItE2',
        'DistList', 'CASSCFEner'
    ]

    for i in DataDirs:
        for j in SubDirs:
            if os.path.exists(os.path.join(str(i), str(j))) is False:
                os.mkdir(os.path.join(str(i), str(j)))


if __name__ == '__main__':
    genDirs()
