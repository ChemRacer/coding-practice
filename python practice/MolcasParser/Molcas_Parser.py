#!/usr/bin/python3
# coding: utf-8
# Import modules
import subprocess
from subprocess import call, check_output
import os
import re
import numpy as np
"""
Molcas Parser

This is a python script for parsing the RASSCF/CASPT2 energies
from OpenMolcas

Long Term Goals:
    -Interface DDCASPT2 with OpenMolcas
    -Retrieve desired data and files from OpenMolcas runs

Grier M. Jones
University of Tennessee, Knoxville
June 2020
"""


class Molcas_Energy_Parse:
    import subprocess
    from subprocess import call, check_output
    import os
    import re
    import numpy as np
    from plumbum.cmd import grep, awk

    def __init__(self, filename, Geo, Basis, Sym, Title, NACTEL, Inactive,
                 RAS2, Symmetry, Spin, orblisting, ITERation, CIMX, SDAV, PRWF,
                 Imag_Shift):
        class InputGen:
            def __init__(self, filename, Geo, Basis, Sym, Title, NACTEL,
                         Inactive, RAS2, Symmetry, Spin, orblisting, ITERation,
                         CIMX, SDAV, PRWF, Imag_Shift):
                def genfile(filename, Geo, Basis, Sym, Title, NACTEL, Inactive,
                            RAS2, Symmetry, Spin, orblisting, ITERation, CIMX,
                            SDAV, PRWF, Imag_Shift):
                    def gen_GateWay(Geo, Basis, Sym):
                        inp = f"""&GATEWAY \n Coord = {Geo} \n Basis = {Basis} \n Group = {Sym} \n End of Input \n"""
                        return inp

                    def gen_Seward():
                        inp = """&SEWARD\n End Of Input \n"""
                        return inp

                    def gen_RASSCF(Title, NACTEL, Inactive, RAS2, Symmetry,
                                   Spin, orblisting, ITERation, CIMX, SDAV,
                                   PRWF):
                        inp = f"""&RASSCF &END \n Title= {Title} \n NACTEL \n {NACTEL} \n Inactive \n {Inactive} \n RAS2 \n {RAS2} \n Symmetry \n {Symmetry} \n Spin \n {Spin} \n orblisting \n {orblisting} \n ITERation \n {ITERation} \n CIMX \n {CIMX} \n SDAV \n {SDAV} \n PRWF \n {PRWF} \n"""
                        return inp

                    def gen_CASPT2(Imag_Shift):
                        inp = f"""&CASPT2 &END \n Imaginary Shift \n {Imag_Shift}"""
                        return inp

                    basename = '.'.join([filename])
                    basename = re.sub('[/()]', '_', basename)
                    Input = '.'.join([basename, 'input'])
                    f = open(Input, 'wb')
                    f.write(gen_GateWay(Geo, Basis, Sym).encode())
                    f.write(gen_Seward().encode())
                    f.write(
                        gen_RASSCF(Title, NACTEL, Inactive, RAS2, Symmetry,
                                   Spin, orblisting, ITERation, CIMX, SDAV,
                                   PRWF).encode())
                    f.write(gen_CASPT2(Imag_Shift).encode())
                    f.close()

                self.run = genfile(filename, Geo, Basis, Sym, Title, NACTEL,
                                   Inactive, RAS2, Symmetry, Spin, orblisting,
                                   ITERation, CIMX, SDAV, PRWF, Imag_Shift)

        class RunMolcas:
            def __init__(self, filename):
                def run(filename):
                    from plumbum.cmd import grep, awk
                    os.environ['MOLCAS']
                    basename = '.'.join([filename])
                    basename = re.sub('[/()]', '_', basename)
                    Input = '.'.join([basename, 'input'])
                    print(f'Input file name: {Input}')
                    open(Input, 'r').readlines()
                    Output = '.'.join([basename, 'output'])
                    print(f'Output file name: {Output}')
                    call(['pymolcas', Input, '-oe', Output])
                    chainPT2 = grep['-i', '::    CASPT2',
                                    Output] | awk['{print " " $7,$8 }']
                    chainRASSCF = grep['-i', '::    RASSCF',
                                       Output] | awk['{print " " $8 }']
                    RASSCF = chainRASSCF()
                    CASPT2 = chainPT2()
                    return CASPT2, RASSCF

                runit = run(filename)
                self.CASPT2 = runit[0]
                self.RASSCF = runit[1]

        PT2Input = str(filename)
        InputGen(PT2Input, OptGeo, Basis, Sym, Title, NACTEL, Inactive, RAS2,
                 Symmetry, Spin, orblisting, ITERation, CIMX, SDAV, PRWF,
                 Imag_Shift).run
        CMD = RunMolcas(PT2Input)
        print(f'RASSCF Energy: {CMD.RASSCF.strip()}')
        print(f'CASPT2 Energy: {CMD.CASPT2.strip()}')
        self.CASPT2 = CMD.CASPT2.strip()
        self.RASSCF = CMD.RASSCF.strip()


if __name__ == '__main__':
    print('Run Test', '\n', 'LiH Example')
    os.chdir('/Users/gjonesresearch/DDCASPT2/MolcasProjects/GeoOpt/LiH/')
    # Gateway
    OptGeo = 'LiH.Opt.xyz'
    Basis = 'ANO-RCC-VDZP'
    Sym = 'nosymm'

    # RASSCF
    Title = 'RASSCF'
    NACTEL = '2 0 0'
    Inactive = '1'
    RAS2 = '2'
    Symmetry = '1'
    Spin = '1'
    orblisting = 'all'
    ITERation = '200 100'
    CIMX = '200'
    SDAV = '500'
    PRWF = '0.01'

    # CASPT2
    Imag_Shift = '0.0'

    # File Name
    PT2Input = 'Yeetus'
    # Build and run files
    z = Molcas_Energy_Parse(PT2Input, OptGeo, Basis, Sym, Title, NACTEL,
                            Inactive, RAS2, Symmetry, Spin, orblisting,
                            ITERation, CIMX, SDAV, PRWF, Imag_Shift)
    # Results
    print(z.CASPT2, z.RASSCF)
    print()
    print('P4 example')
    os.chdir('/Users/gjonesresearch/DDCASPT2/MolcasProjects/GeoOpt/P4/')
    # Gateway
    OptGeo = 'P4.Opt.xyz'
    Basis = 'ANO-RCC-VDZP'
    Sym = 'yz z'

    # RASSCF
    Title = 'RASSCF'
    NACTEL = '12 0 0'
    Inactive = '10 6 2 6'
    RAS2 = '4 3 2 3'
    Symmetry = '1'
    Spin = '1'
    orblisting = 'all'
    ITERation = '200 100'
    CIMX = '200'
    SDAV = '500'
    PRWF = '0.01'

    # CASPT2
    Imag_Shift = '0.0'
    # File Name
    PT2Input = 'Yeetus'
    # Build and run files
    P4 = Molcas_Energy_Parse(PT2Input, OptGeo, Basis, Sym, Title, NACTEL,
                             Inactive, RAS2, Symmetry, Spin, orblisting,
                             ITERation, CIMX, SDAV, PRWF, Imag_Shift)
    # Results
    print(P4.CASPT2, P4.RASSCF)
