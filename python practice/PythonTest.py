#This is the same code as in c++ but written in python
def main():
    # This function asks the user what beer they'd like...
    # in the future it will take the input and give them the beer of their choice
    def choice():
        print("What beer would you like?")
        options = ["PBR", "Coors", "Budweiser"]
        for i in options:
            print(i)


# An example of a function called below
# This function just tells them in what year they'll turn 21

    def garbage(name, year, diff):
        print(name, "can participate in", year + diff, "! \n")

    year = 2020
    age_limit = 21
    #Say what year it is and ask how old
    print("The year is:", year)
    name = input("What is your name? ")
    age = int(input("What is your age? "))
    #Set new_age to age and then determine how many years until 21
    year_diff = age_limit - age
    #An if/else statement to determine by age who can participate
    if age >= age_limit:
        print("You are ", age, " years old \n")
        print("You may participate \n")
        choice()
    else:
        print("You are too young to participate!")
        print("Years until participation", year_diff)
        #Call the new function
        garbage(name, year, year_diff)

    return

if __name__ == "__main__":
    main()
