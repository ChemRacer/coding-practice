#!/usr/bin/env python
# coding: utf-8
from sh.contrib import ssh
import sys
def ssh_interact(content, stdin):
    sys.stdout.write(content.cur_char)
    sys.stdout.flush()

# automatically logs in with password and then presents subsequent content to
# the ssh_interact callback
ssh("double@160.36.203.208", password="voglab04", interact=ssh_interact)
