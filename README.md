This is just a repository for fun little coding practices.
=========
Most of this is going to be FORTRAN, Python, and C++.

---------

1. Note that a lot of the C++ is based off of concepts and code from codecademy's C++ course (https://www.codecademy.com/learn/learn-c-plus-plus)
2. The Game of Life is based on code found on the Rosetta Code website (https://rosettacode.org/wiki/Conway%27s_Game_of_Life#C.2B.2B)
3. Updated 12-16-22
